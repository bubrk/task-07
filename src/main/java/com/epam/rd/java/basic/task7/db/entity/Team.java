package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class Team {

	private int id;

	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Team(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public Team(String name){
		this.name = name;
	}

	public static Team createTeam(String name) {
		return new Team(name);
	}

	@Override
	public String toString() {
		return this.name;
	}

	@Override
	public boolean equals(Object another) {
		if (this == another) return true;
		if (another == null || getClass() != another.getClass()) return false;
		Team anotherTeam = (Team) another;
		return name.equals(anotherTeam.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}
}
