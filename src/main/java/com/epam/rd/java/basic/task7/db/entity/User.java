package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class User {

	private int id;

	private String login;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public User(int id, String login) {
		this.id = id;
		this.login = login;
	}

	public User(String login){
		this.login = login;
	}
	public static User createUser(String login) {
		return new User(login);
	}

	@Override
	public String toString() {
		return this.login;
	}

	@Override
	public boolean equals(Object another) {
		if (this == another) return true;
		if (another == null || getClass() != another.getClass()) return false;
		User anotherUser = (User) another;
		return login.equals(anotherUser.login);
	}

	@Override
	public int hashCode() {
		return Objects.hash(login);
	}
}
