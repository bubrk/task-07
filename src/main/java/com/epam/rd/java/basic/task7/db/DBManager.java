package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;
	private static Properties properties;

	public static final String CONNECTION_URL="jdbc:mysql://127.0.0.1:3306/test2db?user=admin&password=12345678";
	public static final String APP_PROPERTIES="app.properties";
	public static final String SQL_SELECT_ALL_USERS="SELECT * FROM users ORDER BY login ASC";
	public static final String SQL_INSERT_USER="INSERT INTO users VALUES (DEFAULT, ?)";
	public static final String SQL_DELETE_USER="DELETE FROM users WHERE id IN";
	public static final String SQL_GET_USER="SELECT * FROM users WHERE login LIKE ?";
	public static final String SQL_GET_TEAM="SELECT * FROM teams WHERE name LIKE ?";
	public static final String SQL_FIND_ALL_TEAMS="SELECT * FROM teams ORDER BY name ASC";
	public static final String SQL_INSERT_TEAM="INSERT INTO teams VALUES (DEFAULT, ?)";
	public static final String SQL_SET_TEAMS_FOR_USER="INSERT INTO users_teams (user_id, team_id) VALUES";
	public static final String SQL_GET_USER_TEAMS="SELECT team_id, name FROM users_teams JOIN teams ON team_id=teams.id WHERE user_id=?";
	public static final String SQL_DELETE_TEAM="DELETE FROM teams WHERE id=?";
	public static final String SQL_UPDATE_TEAM="UPDATE teams SET name=? WHERE id=?";

	public static synchronized DBManager getInstance() {
		if (instance==null) {
			instance = new DBManager();
			try{
				properties=new Properties();
				properties.load(Files.newInputStream(Path.of(APP_PROPERTIES)));
			} catch (Exception e){
				e.printStackTrace();
			}
		}
		return instance;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		List<User> result = new ArrayList<>();
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			conn = getConnection();
			stmt = conn.prepareStatement(SQL_SELECT_ALL_USERS);

			rs = stmt.executeQuery();

			while (rs.next()){
				result.add(new User(rs.getInt("id"),
									rs.getString("login")));
			}

			rs.close();
			rs = null;

			stmt.close();
			stmt = null;

			conn.close();
			conn = null;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("We have a problem",e);
		} finally {
			// Always make sure result sets and statements are closed,
			// and the connection is returned to the pool
			closeResources(rs, stmt, conn);
		}
		return result;
	}

	public boolean insertUser(User user) throws DBException {
		if (user==null) {
			return false;
		}

		boolean result = false;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			conn = getConnection();

			stmt = conn.prepareStatement(
					SQL_INSERT_USER,
					Statement.RETURN_GENERATED_KEYS);

			int k=0;
			stmt.setString(++k, user.getLogin());

			if (stmt.executeUpdate()>0){
				rs = stmt.getGeneratedKeys();
				if (rs.next()){
					user.setId(rs.getInt(1));
					result = true;
				}
			}

			if (rs!=null){
				rs.close();
				rs = null;
			}

			stmt.close();
			stmt = null;

			conn.close();
			conn = null;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("We have a problem",e);
		} finally {
			// Always make sure result sets and statements are closed,
			// and the connection is returned to the pool
			closeResources(rs, stmt, conn);
		}

		return result;
	}

	public boolean deleteUsers(User... users) throws DBException {

		if (users.length == 0){
			return false;
		}

		Connection conn = null;
		PreparedStatement stmt = null;
		boolean result = false;

		try {
			conn = getConnection();

			String args = Arrays.stream(users)
					.map(u->"?")
					.collect(Collectors.joining(", "," (",")"));

			stmt = conn.prepareStatement(SQL_DELETE_USER+args);

			int k=1;
			for (User user: users) {
				if (user!=null){
					stmt.setInt(k++,user.getId());
				}

			}

			if (stmt.executeUpdate() == users.length ){
				result = true;
			}

			stmt.close();
			stmt = null;

			conn.close();
			conn = null;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("We have a problem",e);
		} finally {
			// Always make sure result sets and statements are closed,
			// and the connection is returned to the pool
			closeResources(null, stmt, conn);
		}
		return result;
	}

	public User getUser(String login) throws DBException {
		if (login==null) {
			return null;
		}

		User result = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			conn = getConnection();
			stmt = conn.prepareStatement(SQL_GET_USER);

			int k=1;
			stmt.setString(k, login);

			rs = stmt.executeQuery();

			if (rs.next()){
				result = new User(rs.getInt("id"),
						rs.getString("login"));
			}

			rs.close();
			rs = null;

			stmt.close();
			stmt = null;

			conn.close();
			conn = null;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("We have a problem",e);
		} finally {
			// Always make sure result sets and statements are closed,
			// and the connection is returned to the pool
			closeResources(rs, stmt, conn);
		}
		return result;
	}

	public Team getTeam(String name) throws DBException {
		if (name==null){
			return null;
		}

		Team result = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			conn = getConnection();
			stmt = conn.prepareStatement(SQL_GET_TEAM);

			int k=1;
			stmt.setString(k, name);

			rs = stmt.executeQuery();

			if (rs.next()){
				result = new Team(rs.getInt("id"),
						rs.getString("name"));
			}

			rs.close();
			rs = null;

			stmt.close();
			stmt = null;

			conn.close();
			conn = null;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("We have a problem",e);
		} finally {
			// Always make sure result sets and statements are closed,
			// and the connection is returned to the pool
			closeResources(rs, stmt, conn);
		}
		return result;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> result = new ArrayList<>();
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			conn = getConnection();
			stmt = conn.prepareStatement(SQL_FIND_ALL_TEAMS);
			//stmt = conn.createStatement();
			rs = stmt.executeQuery();

			while (rs.next()){
				result.add(new Team(rs.getInt("id"),
						rs.getString("name")));
			}

			rs.close();
			rs = null;

			stmt.close();
			stmt = null;

			conn.close();
			conn = null;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("We have a problem",e);
		} finally {
			// Always make sure result sets and statements are closed,
			// and the connection is returned to the pool
			closeResources(rs, stmt, conn);
		}
		return result;
	}

	public boolean insertTeam(Team team) throws DBException {
		if (team==null || team.getName()==null){
			return false;
		}

		boolean result = false;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			conn = getConnection();

			stmt = conn.prepareStatement(
					SQL_INSERT_TEAM,
					Statement.RETURN_GENERATED_KEYS);

			int k=0;
			stmt.setString(++k, team.getName());

			if (stmt.executeUpdate()>0){
				rs = stmt.getGeneratedKeys();
				if (rs.next()){
					team.setId(rs.getInt(1));
					result = true;
				}
			}

			if (rs!=null){
				rs.close();
				rs = null;
			}

			stmt.close();
			stmt = null;

			conn.close();
			conn = null;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("We have a problem",e);
		} finally {
			// Always make sure result sets and statements are closed,
			// and the connection is returned to the pool
			closeResources(rs, stmt, conn);
		}

		return result;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		if (user==null || teams.length == 0){
			return false;
		}

		Connection conn = null;
		PreparedStatement stmt = null;
		boolean result = false;

		try {
			conn = getConnection();
			conn.setAutoCommit(false);

			String args = Arrays.stream(teams)
					.map(t->"(?,?)")
					.collect(Collectors.joining(", "," ",""));

			stmt = conn.prepareStatement(SQL_SET_TEAMS_FOR_USER+args);

			int k=1;
			for (Team team: teams) {
				stmt.setInt(k++,user.getId());
				stmt.setInt(k++,team.getId());
			}

			if (stmt.executeUpdate() == teams.length ){
				result = true;
				conn.commit();
			}

			stmt.close();
			stmt = null;

			conn.close();
			conn = null;

		} catch (SQLException e) {
			e.printStackTrace();

			if (conn!=null){
				try{
					conn.rollback();
				} catch (SQLException ex){
					ex.printStackTrace();
					throw new DBException("Can't roll back!",ex);
				}
			}

			throw new DBException("We have a problem",e);
		} finally {
			// Always make sure result sets and statements are closed,
			// and the connection is returned to the pool
			closeResources(null, stmt, conn);
		}
		return result;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> result = new ArrayList<>();
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			conn = getConnection();
			stmt = conn.prepareStatement(SQL_GET_USER_TEAMS);

			int k=1;
			stmt.setInt(k,user.getId());

			rs = stmt.executeQuery();

			while (rs.next()){
				result.add(new Team(rs.getInt("team_id"),
						rs.getString("name")));
			}

			rs.close();
			rs = null;

			stmt.close();
			stmt = null;

			conn.close();
			conn = null;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("We have a problem",e);
		} finally {
			// Always make sure result sets and statements are closed,
			// and the connection is returned to the pool
			closeResources(rs, stmt, conn);
		}
		return result;
	}

	public boolean deleteTeam(Team team) throws DBException {

		Connection conn = null;
		PreparedStatement stmt = null;
		boolean result = false;

		try {
			conn = getConnection();

			stmt = conn.prepareStatement(SQL_DELETE_TEAM);

			int k=0;
			stmt.setInt(++k,team.getId());

			if (stmt.executeUpdate() >0){
				result = true;
			}

			stmt.close();
			stmt = null;

			conn.close();
			conn = null;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("We have a problem",e);
		} finally {
			// Always make sure result sets and statements are closed,
			// and the connection is returned to the pool
			closeResources(null, stmt, conn);
		}
		return result;
	}

	public boolean updateTeam(Team team) throws DBException {

		Connection conn = null;
		PreparedStatement stmt = null;
		boolean result = false;

		try {
			conn = getConnection();

			stmt = conn.prepareStatement(SQL_UPDATE_TEAM);

			int k=0;
			stmt.setString(++k,team.getName());
			stmt.setInt(++k,team.getId());

			if (stmt.executeUpdate() >0){
				result = true;
			}

			stmt.close();
			stmt = null;

			conn.close();
			conn = null;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("We have a problem",e);
		} finally {
			// Always make sure result sets and statements are closed,
			// and the connection is returned to the pool
			closeResources(null, stmt, conn);
		}
		return result;
	}



	private Connection getConnection() throws SQLException {
		return DriverManager.getConnection(properties.getProperty("connection.url"));
	}

	private void closeResources(ResultSet rs, PreparedStatement stmt, Connection conn){

		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e){
				e.printStackTrace();
			}
		}

		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e){
				e.printStackTrace();
			}
		}

		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e){
				e.printStackTrace();
			}
		}

	}

}